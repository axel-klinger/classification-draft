from internationalization.Translator import Translator

translator = Translator()

# languages = ['en','es','fr','it','pl','pt'] # 7 (inkl. de) meistgesprochene EU Sprachen
# languages = ['cs','el','en','es','fi','fr','it','sk','sv'] # all EULiST languages
languages = ['cs','da','el','en','es','et','fi','fr','hr','hu','it','nl','pl','pt','sk','sl','sv'] # 17 EULiST+ Sprachen (ohne bg, ga, lt, lv, mt, ro, uk)
# languages = ['bg','cs','da','el','en','es','et','fi','fr','ga','hr','hu','it','lt','lv','mt','nl','pl','pt','ro','sk','sl','sv','uk'] # 23 + de + uk = alle EU Sprachen

def no_trans(pair):
    k, v = pair
    if v != '':
        return False
    else:
        return True

coverage = 0.0
count = 0

with open('subjects.txt', 'r', encoding='utf-8') as file:
    subjects = file.readlines()
    for s in subjects:
        subject = s.strip()
        translation = translator.translate(subject, languages)
        # print(s.rstrip() + " : " + str(translation) + " : " + str(len(list(filter(None, translation.values())))))
        print(s.rstrip() + " :\t " + str(len(list(filter(None, translation.values())))) + " : " + ','.join(dict(filter(no_trans, translation.items())).keys()))
        # print(s.rstrip() + " :\t " + str(len(list(filter(None, translation.values())))))

        coverage += len(list(filter(None, translation.values()))) / len(languages)
        count +=1

print(f'Translatability: {round(round(coverage/count, 3)*100, 1)}%')