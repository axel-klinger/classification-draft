# Classification Draft

The aim of this project is to outline a classification of study subjects/fields of science which should, if possible, be available in all European (official) languages. The basis for the translation into these languages will be the mapping of Wikidata.

* [Languages of the European Union](https://de.wikipedia.org/wiki/Amtssprachen_der_Europ%C3%A4ischen_Union) (german version includes language codes)

## Getting started

The OERSI [internationalisation tool](https://gitlab.com/oersi/internationalization-tool) is used to query mappings.

* clone the [internationalisation tool](https://gitlab.com/oersi/internationalization-tool)
  * set first three values in config/config.yml to be allowed to query wikidata
    * client-identifier
    * client-version
    * client-contact-information
```
  pip install requirements.txt
```

* clone this repository
  * copy all files into internationalisation-tool folder
* in internationalisation-tool
  * test run

```
python WordTranslator.py
```

Feel free to use any of those text files, or your own, to check translatability. Just change filename in WordTranslator.py

Even try out different sets of languages from most spoken to all EU languages and compare the results.

## Create Tutrle file from indented list of vocabulary values

in internationalisation-tool
```
python create-turtle-with-translation.py
```
This will create a vocabulary.ttl from subjects.txt

## Translatability

* 7 = most spoken languages in EU (de, en, es, fr, it, pl, pt) 
* 10 = all EULiST languages (cs, de, el, en, es, fi, fr, it, sk, sv)
* 18 = EULIST, most spoken and some other (cs, da, de, el, en, es, et, fi, fr, hr, hu, it, nl, pl, pt, sk, sl, sv)
* 25 = all EU languages + uk (18 + bg, ga, lt, lv, mt, ro, uk) 


Average percentage of translatability over all fields

| Spec | 7 | 10 | 18 | 25 |
|------|---|----|----|----|
|HFS *|63.5|59.9|58.5|54.5|
|DFG *|53.9|52.2|51.1|48.2|
|OEFOS *|64.8|59.3|56.4|51.0|
|SUBJECTS|99.0|97.7|97.3|93.9|
|LANG|100|100|100|100|
|TYPES|100|98.6|98.5|95.8|

(*) alot of false positives because of composed terms
