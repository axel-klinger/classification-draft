
title_de = "Meine Klassifikation"
title_en = "My Classification"
alternative_en = "Simple Subjects"
description_de = "Eine neue Fächerklassifikation für Bildung - frei, offen, einfach, international"
description_en = "A new subject classification for education - free, open, simple, international"
publisher = "https://oersi.org"
prefix = "ESC" # educational subjet classification
url = "https://gitlab.com/TIBHannover/course-catalogue/subject-classification/-/blob/main/subjects.txt"
date = "2024-07-11"
file_name = "subjects.txt"
output_file_name = "vocabulary.ttl"

list = []
stack = []
subject = {}
num = 0
level_2 = '    '
level_3 = '        '

with open(file_name, 'r', encoding='utf-8') as file:
    lines = file.readlines()
    for line in lines:
        num += 1
        subject_name = line.strip()
        level = 3 if line.startswith(level_3) else 2 if line.startswith(level_2) else 1 
        subject = {"id" : num, "level" : level, "label" : {"de" : subject_name, "en" : subject_name}, "narrower" : [], "broader" : None}

        if len(stack) == 0:
            stack.append(subject)
        elif stack[-1]["level"] < subject["level"]:
            # stack.append({"id":subject["id"], "level":subject["level"]})
            stack.append(subject)
            subject["broader"] = stack[-2]["id"]
            stack[-2]["narrower"].append(subject["id"])
        elif stack[-1]["level"] == subject["level"]:
            list.append(stack.pop())
            # stack.append({"id":subject["id"], "level":subject["level"]})
            stack.append(subject)
            subject["broader"] = stack[-2]["id"]
            stack[-2]["narrower"].append(subject["id"])
        else:
            list.append(stack.pop())
            while len(stack) > 0 and stack[-1]["level"] >= subject["level"]:
                list.append(stack.pop())
            # stack.append({"id":subject["id"], "level":subject["level"]})
            stack.append(subject)
            if len(stack) > 1:
                subject["broader"] = stack[-2]["id"]
                stack[-2]["narrower"].append(subject["id"])
            else:
                pass                

        # print("Stack: " + str(stack))
        print("Subject: " + str(subject))
        

turtle = f"""
@base <{url}> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#>.
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix schema: <http://schema.org/> .
@prefix vann: <http://purl.org/vocab/vann/> .

<scheme> a skos:ConceptScheme ;
  dct:title "{title_de}"@de, "{title_en}"@en ;
  dct:alternative "{alternative_en}"@en ;
  dct:description "{description_de}"@de , "{description_en}"@en ;
  dct:issued "{date}" ;
  dct:publisher <{publisher}> ;
  vann:preferredNamespacePrefix "{prefix}" ;
  vann:preferredNamespaceUri "{url}" ;
  skos:hasTopConcept  {', '.join('<n' + str(x["id"]) + '>' for x in list if x["level"] == 1)} .

"""
for l in list:
    # print(l)
    turtle += f"<n{l['id']}> a skos:Concept ;\n"
    if len(l["label"]) > 0:
        turtle += f"  skos:prefLabel {', '.join("\"" + y + "\"@" + x for x, y in l["label"].items())} ;\n"
    if len(l["narrower"]) > 0:
        turtle += f"  skos:narrower {', '.join("<n" + str(x) + ">" for x in l["narrower"])} ;\n"
    if l["broader"] != None:
        turtle += f"  skos:broader {l["broader"]} ;\n"
    turtle += f"  skos:notation \"{l['id']}\" ;\n"
    if l["level"] == 1:
        turtle += f"  skos:topConceptOf <scheme> .\n\n"
    else:
        turtle += f"  skos:inScheme <scheme> .\n\n"

print(turtle)

with open(output_file_name, 'w', encoding='utf-8') as out:
    out.write(turtle)